﻿using System.Collections.Generic;
using DL.DomainModel.Entities;
namespace DL.Interfaces.Repository
{
    public interface IUserRepository
    {
        User GetUserById(int id);
        User GetUserByEmail(string email);
        ICollection<User> GetAllUsers();
        bool CheckExistUser(string email);
        User GetRegistredUser(string email, string password);
    }
}