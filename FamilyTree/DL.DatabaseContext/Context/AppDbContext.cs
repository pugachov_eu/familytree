﻿using System;
using DL.DomainModel.Entities;
using Microsoft.EntityFrameworkCore;
using DL.Interfaces.DatabaseContext;
using Microsoft.EntityFrameworkCore.Storage;

namespace DL.DatabaseContext.Context
{
    public class AppDbContext: DbContext, IAppDbContext
    {
        public DbSet<User> Users { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

    }
}
