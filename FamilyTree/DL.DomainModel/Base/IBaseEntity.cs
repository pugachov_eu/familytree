﻿namespace DL.DomainModel.Base
{
    public interface IBaseEntity
    {
        int Id { get; set; }
    }
}
