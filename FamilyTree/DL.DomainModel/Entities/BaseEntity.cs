﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DL.DomainModel.Base;

namespace DL.DomainModel.Entities
{
    public class BaseEntity :IBaseEntity
    {
        public int Id { get; set; }
    }
}
