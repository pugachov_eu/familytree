﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DL.DomainModel.Base;
using DL.DomainModel.Entities;
using DL.Interfaces.DatabaseContext;
using DL.Interfaces.UoW;
using Microsoft.EntityFrameworkCore;

namespace DL.UnitOfWork
{
    public class UnitOfWork :IUnitOfWork
    {
        private readonly IAppDbContext _appDbContext;

        public UnitOfWork(IAppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public virtual TEntity Add<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            return GetDbSet<TEntity>().Add(entity).Entity;
        }

        public virtual void Add<TEntity>(ICollection<TEntity> entities) where TEntity : class, IBaseEntity
        {
            GetDbSet<TEntity>().AddRange(entities);
        }

        public virtual void Update<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            var dbContext = GetDbContext<TEntity>();

            var dbEntity = dbContext.Set<TEntity>().Single(t => t.Id == entity.Id);

            dbContext.Entry(dbEntity).CurrentValues.SetValues(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            var dbSet = GetDbSet<TEntity>();

            dbSet.Attach(entity);

            dbSet.Remove(entity);
        }

        public virtual void Delete<TEntity>(ICollection<TEntity> entities) where TEntity : class, IBaseEntity
        {
            var dbSet = GetDbSet<TEntity>();

            foreach (var entity in entities)
            {
                dbSet.Attach(entity);
            }

            dbSet.RemoveRange(entities);
        }
        public virtual void SaveChanges()
        {
            _appDbContext.SaveChanges();
        }
        protected IDbContext GetDbContext<TEntity>()
        {
            if (typeof(BaseEntity).IsAssignableFrom(typeof(TEntity))) return _appDbContext;

            throw new InvalidOperationException("The database context not found for entity " + typeof(TEntity).FullName);
        }
        protected DbSet<TEntity> GetDbSet<TEntity>() where TEntity : class, IBaseEntity
        {
            return GetDbContext<TEntity>().Set<TEntity>();
        }

    }
}