﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.BusinessModel.Models;
using BL.Interfaces.Services;
using DL.DomainModel.Entities;
using DL.Interfaces.Repository;
using DL.Interfaces.UoW;

namespace BL.Services.Services
{
    public class UserService:IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IUserRepository userRepository, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
            _mapper = mapper;
        }
        public void Register(UserModel model)
        {
            var user = _mapper.Map<User>(model);

            _unitOfWork.Add(user);
            _unitOfWork.SaveChanges();
        }

        public UserModel GetUserById(int id)
        {
            var user = _mapper.Map<UserModel>(_userRepository.GetUserById(id));
            return user;
        }

        public UserModel GetUserByEmail(string email)
        {
            var user = _mapper.Map<UserModel>(_userRepository.GetUserByEmail(email));
            return user;
        }

        public bool UpdateUser(UserModel model)
        {
            try
            {   
                var user = _mapper.Map<User>(model);
                user.Password = _userRepository.GetUserById(model.Id).Password; 
                _unitOfWork.Update(user);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public ICollection<UserModel> GetAllUsers()
        {
            var users = _mapper.Map<ICollection<UserModel>>(_userRepository.GetAllUsers());
            return users;
        }

        public bool DeleteUserByEmail(string email)
        {
            try
            {
                var model = _userRepository.GetUserByEmail(email);
                var user = Mapper.Map<User>(model);
                _unitOfWork.Delete(user);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Login(UserModel model)
        {
            var user = _userRepository.GetRegistredUser(model.Email, model.Password);
            return user != null;
        }

        public bool UserExists(string email)
        {
            return _userRepository.CheckExistUser(email);
            
        }
    }
}
