﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.BusinessModel.Models;
using BL.Infrastructure.Configurations;
using DL.DomainModel.Entities;
using WebApplication.Models;

namespace WebApplication.App_Config
{
    public class AutomapperConfiguration : Profile
    {
        protected override void Configure()
        {
            CreateMap<UserViewModel, UserModel>();
            CreateMap<UserModel, UserViewModel>();

            CreateMap<RegisterViewModel, UserModel>();
            CreateMap<UserModel, RegisterViewModel>();

            CreateMap<LoginViewModel, UserModel>();
            CreateMap<UserModel, LoginViewModel>();
        }
    
    }
}
