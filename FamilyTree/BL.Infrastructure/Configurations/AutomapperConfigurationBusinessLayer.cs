﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.BusinessModel.Models;
using DL.DomainModel.Entities;

namespace BL.Infrastructure.Configurations
{
    public class AutomapperConfigurationBusinessLayer : Profile
    {
        protected override void Configure()
        {
            CreateMap<UserModel, User>();
            CreateMap<User, UserModel>();
        }
       
    }
}
